enyo.kind({
	name: "CompassScene",
	//kind: "FittableRows",
  //kind: 'Panels',
	classes: "enyo-unselectable enyo-fit bk",
	//arrangerKind: "CollapsingArranger",
	components: [
      {kindLayout: "FittableRowsLayout", components:[
        {kind: "onyx.Toolbar", components: [
          {kind: "onyx.IconButton", src:"assets/images/common-back.png", ontap:"back"}
        ]},

        {name: "compass", fit: true, classes: "compass", components:[
          {name: "arrow", kind: "Image"},
        ]},
        {name: "distance", classes: "compass-distance"},
        {kind: "onyx.Groupbox", components: [
          {name: "header", kind: "onyx.GroupboxHeader"},
          {name:"text"}
        ]},
        {tag:"br"},
        {kind: "onyx.Groupbox", components: [
          {kind: "onyx.GroupboxHeader", content: "Your comment" },
          {layoutKind:"FittableColumnsLayout",components:[
            {name:"mynote", allowHtml:true,style:"padding:16px"}
          ]},
        ]},
        {layoutKind:"FittableColumnsLayout",components:[
          {name:"addNote", kind: "onyx.Button", fit:true,content: "Add Comment", ontap: "showPopup", classes:"onyx-affirmative"}
        ]},
        {name:"gpsTimeout",content:""}
      ]},
      {name: "popup", kind: "onyx.Popup", classes:"popup", centered: true, autoDismiss:false, modal: true, floating: true, onShow: "popupShown", components: [
				{kind: "onyx.InputDecorator", components: [
					{kind: "onyx.TextArea", name:"note"}
				]},
				{tag: "br"},
				{kind: "onyx.Button", style:"float:left", content: "Cancel", ontap: "closePopup"},
				{kind: "onyx.Button", classes:"onyx-affirmative",style: "float:right", content: "Ok", ontap: "addNote"}
			]},
  ],
  back: function(){
    this.stopGeowatching();
    if(this.screenLock)
    {
      this.screenLock.unlock();
      this.screenLock=0;
    }
    //this.prevScene.renderInto(document.body);
    this.prevScene();
  },
  rendered: function(){
    this.inherited(arguments);
    //this.start();
  },
  startGeowatching: function() {
    this.job=navigator.geolocation.watchPosition(enyo.bind(this, "updatePosition"),enyo.bind(this,"getPositionFailed"),{timeout:30000});
    this.$.gpsTimeout.setContent("Waiting for GPS...");
    this.$.gpsTimeout.shown=1;
  },
  stopGeowatching: function() {
    navigator.geolocation.clearWatch(this.job);
  },
  updatePosition: function(pos) {
    if(this.$.gpsTimeout.shown)
      this.$.gpsTimeout.setContent("");
    LOCATION.lon=pos.coords.longitude;
    LOCATION.lat=pos.coords.latitude;
    var angle;
    if(isNaN(pos.coords.heading))
    {
      angle=0;
      DIRECTION.lat=0;
      DIRECTION.lon=0;
    }
    else
    {
      angle=pos.coords.heading/180*Math.PI;
      DIRECTION.lat=Math.cos(angle);
      DIRECTION.lon=Math.sin(angle);
    }
    this.target.calcDistance(LOCATION);
    this.target.calcDirection(LOCATION,DIRECTION);
    this.$.distance.setContent(this.target.distance+" m");
    this.$.arrow.setSrc(this.target.image);
  },
  getPositionFailed: function(e) {
    var message=0;
    this.$.arrow.setSrc('assets/images/arrows/00.png');
    switch(e.code)
    {
      case 1:message="Permission denied\nPlease allow the program to use geolocation";break;
      case 2:message="Impossible to get your geolocation. Is GPS enabled?";break;
      case 3:this.$.gpsTimeout.setContent("GPS timeout. Please wait...");this.$.gpsTimeout.shown=1;break;
      default: message="Unknown GPS error: "+e.code;
    }
    if(message)alert(message);
    this.$.download.setDisabled(false);
    this.$.log.setContent("");
  },
  setTarget: function(target){
    this.target=target;
  },
  setPrev: function(p) {
    this.prevScene=p;
  },
  start: function(){
    this.$.arrow.setSrc('assets/images/arrows/00.png');
    this.$.header.setContent("Note from "+this.target.name);
    this.$.text.setContent(this.target.text);
    this.$.mynote.setContent(this.target.mynote);
    this.$.gpsTimeout.shown=0;
    this.startGeowatching();
    if((!this.screenLock)&&window.navigator.requestWakeLock)
      this.screenLock = window.navigator.requestWakeLock('screen');
  },
  showPopup: function() {
    this.$.popup.show();
    this.$.note.focus();
  },
  popupShown: function() {
    this.$.note.setValue(this.target.mynote);
  },
  closePopup: function() {
    this.$.popup.hide();
  },
  addNote:function() {
    this.target.mynote=this.$.note.value;
    this.$.mynote.setContent(this.target.mynote);
    saveSettings();
    this.$.popup.hide();
  }
});


var DB_VERSION=1;
var VERSION="v0.3.2";
var PAYPAL_BUTTON='<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=7CNM3U9DVEGDS&lc=RU&item_name=Shura0&item_number=geobug_mozilla&currency_code=USD&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted" target=_blank><img src="https://www.paypalobjects.com/en_US/RU/i/btn/btn_donateCC_LG.gif"></a>';
var ABOUT_TEXT='<div style="text-align:center"><b>OSM Hunter '+VERSION+'</b><br></div><div>The program is written by Alezander Zaitsev <a href="mailto:shura0@yandex.ru">shura0@yandex.ru</a> and distributed under BSD license.<br>\
If you like the program please donate<br></div><div style="text-align:center">'+PAYPAL_BUTTON+"</div>";

var HELP_TEXT="The program gets nearest notes from <a href='http://openstreetmap.org' target=_blank>openstreetmap.org</a> and displays distance and direction to they.<br> For furter info please look <a href='http://wiki.openstreetmap.org/wiki/Notes' target=_blank>openstreetmap wiki...</a>";
function saveSettings() {

  var db = window.indexedDB || window.webkitIndexedDB; ;
  if(!db)
  {
    alert("Warning!\nImpossible to save settings");
    return;
  }
  console.log("SaveSettings...");
  run=function()
  {
    console.log("RUN");
    var req = db.open("SETTINGS", DB_VERSION);
    req.onupgradeneeded=function(e){
      console.log("upgrade needed!");
      var db=e.target.result;
      if(!db.objectStoreNames.contains("radius"))
        var store = db.createObjectStore("radius",{keyPath: "radius"});
      if(!db.objectStoreNames.contains("targets"))
        var store = db.createObjectStore("targets",{keyPath:"id"});
      if(!db.objectStoreNames.contains("filter"))
        var store = db.createObjectStore("filter",{keyPath:"filter"});
    };
    req.onsuccess=function(event){
      console.log("Database opened!");
      var db=event.target.result;
      try{
        var trans = db.transaction("radius", "readwrite");
      }
      catch(e)
      {
        console.log("Error: "+e.name);
        console.log("trying to increase version");
        DB_VERSION++;
        saveSettings();
        return;
      }
      var store = trans.objectStore("radius");
      store.clear();
      var request = store.put({radius:SETTINGS.radius});
      request.onsuccess = function(e) {
          console.log("radius updated");
      }
      try{
        trans = db.transaction("filter", "readwrite");
      }
      catch(e) {
        console.log("Error: "+e.name);
        console.log("trying to increase version");
        DB_VERSION++;
        saveSettings();
        return;
      }
      store = trans.objectStore("filter");
      store.clear();
      var request = store.put({filter:SETTINGS.filter});
      request.onsuccess = function(e) {
          console.log("filter updated");
      }
      trans = db.transaction("targets", "readwrite");
      store = trans.objectStore("targets");
      store.clear();
      var req;
      for(var i=0;i<TARGETS.length;i++)
      {
        req=store.put({target:TARGETS[i],id:TARGETS[i].id});
        req.onsuccess=function(a){
          console.log("saved target");
        }
      }
    };
    req.onerror= function(event)
    {
      console.log("Impossible to open base "+req.error.name);
      if(req.error.name == "VersionError")
      {
        db.deleteDatabase("SETTINGS");
        saveSettings();
      }
    }
  }
  if(TARGETS.length<1)
  {
    console.log("deleting db...");
    DB_VERSION=1;
    var dd=db.deleteDatabase("SETTINGS");
    dd.onsuccess=run;
    dd.onerror=function(){alert("Cannot delete db")};
  }
  else
    run();
}

function loadSettings(done) {//callback function

  var db = window.indexedDB || window.webkitIndexedDB; ;
  //var dd=db.deleteDatabase("SETTINGS");
  //return;
  if(!db)
  {
    TARGETS=[];
    SETTINGS.radius=10;
    SETTINGS.filter="";
    done(0);
    return;
  }
  console.log("Load settings...");
  var req = db.open("SETTINGS", DB_VERSION);
  req.onsuccess=function(e){
    SETTINGS.targets=[];
    TARGETS=[];
    var db=e.target.result;
    try {
      var trans = db.transaction("radius", "readonly");
      var store = trans.objectStore("radius");
      var cursorRequest = store.openCursor();
      cursorRequest.onsuccess = function(e) {
        console.log("Cursor requested for radius");
        var result = e.target.result;
        if(!result) return;
        console.log(result.value);
        SETTINGS.radius=result.value.radius;
        result.continue();
      };
    }
    catch(e)
    {
      console.log("Cannot read radius");
      SETTINGS.radius=10;
    }
    try {
      var trans = db.transaction("filter", "readonly");
      var store = trans.objectStore("filter");
      var cursorRequest = store.openCursor();
      cursorRequest.onsuccess = function(e) {
        console.log("Cursor requested for filter");
        var result = e.target.result;
        if(!result) return;
        console.log(result.value);
        SETTINGS.filter=result.value.filter;
        result.continue();
      };
    }
    catch(e)
    {
      console.log("Cannot read filter string");
      SETTINGS.filter="";
    }
    try{
      var trans = db.transaction("targets", "readonly");
      var store = trans.objectStore("targets");
      var keyRange = IDBKeyRange.lowerBound(0);
      var cursorRequest = store.openCursor(keyRange);
      cursorRequest.onsuccess = function(e) {
        console.log("Cursor requested for targets");
        var result = e.target.result;
        if(!result)
        {
          done(0);
          return;
        }
        var a=result.value.target;
        SETTINGS.targets.push(a);
        console.log(result.value);
        var t=new Target();
        t.copyFrom(a);
        if(t.mynote)
        {
          t.styleclass="withnote";
        }
        if(!LOCATION.lat && !LOCATION.lon)
        {
          t.distance=0;
          t.direction=0;
          t.image='assets/images/arrows/00.png';
        }
        TARGETS.push(t);
        result.continue();
      };
    }
    catch(e){
      //alert("Cannot read settings");
      console.log("Cannot read list of targets");
      SETTINGS.targets=[];
      done(1);
      return;
    }
  };
  req.onerror=function(e)
  {
    //console.error(JSON.stringify(e));
    //var dd=db.deleteDatabase("SETTINGS");
    //alert("Internal error\n"+req.error);
    console.log("Cannot open settings");
    console.log("Internal error:"+ req.error.name);
    if(req.error.name == "VersionError")
    {
      if(DB_VERSION<5)
      {
        DB_VERSION++;
        console.log("Reopen base with version "+DB_VERSION);
        loadSettings(done);
        return;
      }
      else
      {
        console.log("Impossible to read base. Deleting...");
        var ret=db.deleteDatabase("SETTINGS");
        ret.onsucces=function(result)
        {
          console.log("Base deleted");
          SETTINGS.targets=[];
          SETTINGS.radius=10;
          SETTINGS.filter="";
          done(1);
        };
        ret.onerror=function(result)
        {
          console.error("Unexpected error. Impossible to delete base. It's FAIL!");
        }
      }

    }
    else
    {
      SETTINGS.targets=[];
      SETTINGS.radius=10;
      SETTINGS.filter="";
      done(1);
    }
  }
}

function makeRequest(distance)
{
  var dlat=distance/111; //km per degree latitude
  //var dlon=distance/62; //km per degree longitude
  var lat_rad = LOCATION.lat * Math.PI / 180;
  var dlon=distance/(111*Math.cos(lat_rad));
  return 'http://api.openstreetmap.org/api/0.6/notes.json?bbox='+(LOCATION.lon-dlon)+','+(LOCATION.lat-dlat)+','+(LOCATION.lon+dlon)+','+(LOCATION.lat+dlat)+'&closed=0';
}

enyo.kind({
	name: "ToolsScene",
  events: {
		onBack: ""
	},
  published: {
		importance: 0
	},
	kind: "FittableRows",
	classes: "enyo-unselectable enyo-fit bk",
	components: [
		{layoutKind: "FittableRowsLayout", components: [
			{kind: "onyx.Toolbar", components: [
        {kind: "onyx.IconButton", src:"assets/images/remove-icon.png", ontap:"back"},
        {content:"Download notes", style:"text-align:center"}
			]},
      {kind: "onyx.Groupbox", components: [
        {name: "header", kind: "onyx.GroupboxHeader", content1: "Download notes"},
        {kind: 'FittableRows', components:[
          {layoutKind:"FittableColumnsLayout", components:[
            {classes: "onyx-toolbar-inline label", components: [
              {content:"Radius:"},
              {kind: "onyx.PickerDecorator", components: [
                {style: "min-width: 60px;"},
                {kind: "onyx.IntegerPicker", name:"radius", onchange:"radiusChanged", min: 5, max: 60, value: 5},
              ]},
              {content:" km"}
            ]},
          ]},
          {layoutKind:"FittableColumnsLayout", components:[
            {classes: "onyx-toolbar-inline label", components: [
              {kind: "onyx.Checkbox", name:"checkbox",onchange: "checkboxClicked"},
              {content:"Filter by name:"},
              {kind: "onyx.InputDecorator", components: [
                {kind: "onyx.Input", name:"nameField", disabled:true, placeholder: "Enter nick..."}
              ]}
            ]},
          ]},

        ]},
      ]},
      {layoutKind:"FittableColumnsLayout",components:[
        {kind: "onyx.Button", name:"download", content:"Download", fit:true, ontap:"getList"},
      ]},
      {name: "nonotesPopup", kind: 'onyx.Popup', centered: true, floating: true, modal:true, classes:"onyx-sample-popup", style: "padding: 10px;", content: "Notes not found"},
      {name:'log', allowHtml:true}
		]},
	],
  rendered: function(){
    this.inherited(arguments);
    this.start();
  },
  start:function()
  {
    this.$.nameField.setAttribute("disabled",!this.$.checkbox.getValue());
    this.$.nameField.setValue(SETTINGS.filter);
    this.$.radius.setValue(SETTINGS.radius);
    this.$.log.setContent("");
  },
  checkboxClicked: function(a) {
    this.$.nameField.setAttribute("disabled",!a.getValue());
  },
  radiusChanged: function(e) {
    SETTINGS.radius=e.content;
  },
  getList: function() {
    this.$.download.setDisabled(true);
    this.$.log.addContent("Waiting for GPS...");
    navigator.geolocation.getCurrentPosition(enyo.bind(this,"refreshPosition"),enyo.bind(this,"getPositionFailed"),{timeout:10000});
  },
  getPositionFailed: function(e) {
    var message;
    switch(e.code)
    {
      case 1:message="Permission denied\nPlease allow the program to use geolocation";break;
      case 2:message="Impossible to get your geolocation. Is GPS enabled?";break;
      case 3:message="GPS timeout.\nImpossible to get your position, GPS does not work in buildings, please get outside and try again";break;
      default: message="Unknown GPS error: "+e.code;
    }
    alert(message);
    this.$.download.setDisabled(false);
    this.$.log.setContent("");
  },
  refreshPosition: function(pos) {
    LOCATION.lat=pos.coords.latitude;
    LOCATION.lon=pos.coords.longitude;
    var log=this.$.log;
    var THIS=this;
    SETTINGS.radius=this.$.radius.getValue();
    console.log("Radius is "+SETTINGS.radius);
    log.addContent('ok<br>');
    log.addContent('Getting notes list...');
    var xmlhttp = new XMLHttpRequest();
    var request=makeRequest(SETTINGS.radius);
    xmlhttp.open('GET', request, true);
    xmlhttp.onreadystatechange = function(){
      if (xmlhttp.readyState == 4) {
        //Mojo.Log.info("readystate=4");
        if (xmlhttp.status == 200) {
          //Mojo.Log.info("status=200");
          log.addContent('ok<br>');
          var t=xmlhttp.responseText;
          //Mojo.Log.info("Received text="+t);
          var notes=JSON.parse(t);
          //Mojo.Log.info("Type is:",notes.type);
          for(var j=0;j<SETTINGS.targets.length;j++)
          {
            if(SETTINGS.targets[j] && !SETTINGS.targets[j].mynote)
            {
              SETTINGS.targets.splice(j,1);
              j--;
            }
          }
          if(!THIS.$.nameField.value)
            THIS.$.nameField.value="";
          console.log("checkbox="+THIS.$.checkbox.checked);
          console.log("nameField="+THIS.$.nameField.value.toUpperCase());
          SETTINGS.filter=THIS.$.nameField.value.toUpperCase();
          for(var i=0;i<notes.features.length;i++)
          {
            var n=notes.features[i];
            if(notes.features[i].type=='Feature')
            {
              if(n.geometry && n.geometry.coordinates && n.geometry.type=="Point")
              {
                var t=new Target(n.geometry.coordinates[1],n.geometry.coordinates[0]);
                t.id=n.properties.id;
                t.setText(n.properties.comments[0].text);
                var name=n.properties.comments[0].user;
                if(!name)name="Anonimous";
                t.calcDistance(LOCATION);
                t.setName(name);
                t.calcDirection(LOCATION,DIRECTION);
                var flag=0;
                for(var j=0;j<TARGETS.length;j++)
                {
                  if(TARGETS[j].id == t.id)
                  {
                    flag=1;
                    break;
                  }
                }
                if(!flag && (t.distance/1000<SETTINGS.radius))
                {
                  if(THIS.$.checkbox.checked)
                  {
                    if(SETTINGS.filter == name.toUpperCase())
                      TARGETS.push(t);
                  }
                  else
                    TARGETS.push(t);
                }
              }
            }
          }
          for(var i=0;i<TARGETS.length;i++)
          {
            var len=Distance(LOCATION,TARGETS[i].point);
          }
          TARGETS.sort(function(a,b){return a.distance-b.distance});
          saveSettings();
          THIS.$.download.setDisabled(false);
          if(TARGETS.length<1)
          {
            THIS.$.nonotesPopup.show();
            THIS.$.log.setContent("");
          }
          else
          {
            THIS.back();
          }
        }
        else {
          switch(xmlhttp.status)
          {
            case 0:
              //Mojo.Controller.errorDialog("Невозможно подключиться к сервру");
            break;
            case 403:
              //Mojo.Controller.errorDialog("Forbidden<br>Неверный логин или пароль"+xmlhttp.statusText);
            break;
            case 404:
              //Mojo.Controller.errorDialog("Not found<br>Something went wrong"+xmlhttp.statusText);
            break;
            default:
              ///Mojo.Controller.errorDialog(xmlhttp.status+"<br>"+xmlhttp.statusText);
            break;
          }
          //buttonModel.disabled=false;
          //this.oldButtonModel.disabled=false;
          //controller.modelChanged(buttonModel);
          //controller.modelChanged(this.oldButtonModel);
          //controller.get('downloadButton').mojo.deactivate();
          log.addContent('fail<br>');
          //controller.get('text').update(text);
          THIS.$.download.setDisabled(false);
        }
      }
    }
    xmlhttp.send();
  },
  setPrev: function(p){
    this.prevScene=p;
  },
  back: function() {
    //this.prevScene.renderInto(document.body);
    //this.doBack();
    this.prevScene();
    return true;
  }
  });
var TARGETS=[];
var SETTINGS={};
var LIST;
var POPUP;
var COMPASS;
var TOOLS;
var APP;
var READYSTATE;
//var LOCATION=new Point(0,0);
var LOCATION=new Point(0,0);
var DIRECTION=new Point(0,0);
//lat=55.8523&lon=37.6473
//var REQUEST='http://api.openstreetmap.org/api/0.6/notes.json?bbox='+(LOCATION.lon-1)+','+
//(LOCATION.lat-1)+','+(LOCATION.lon+1)+','+(LOCATION.lat+1)+'&closed=0';
//loadSettings();


enyo.kind({
	name: "App",
  kind: "FittableRows",
  classes: "enyo-unselectable enyo-fit",
  components: [
    {name: "popup", kind: 'onyx.Popup', centered: true, floating: true, modal:true, classes:"onyx-sample-popup", style: "padding: 10px;", content: "Loading..."},
  ],
  create: function() {
    this.inherited(arguments);
  },
  rendered: function(){
    this.inherited(arguments);
    this.$.popup.show();
  },
  done: function(firstrun) {
    console.log("first run? "+firstrun);
    this.$.popup.hide();
    if(!LIST)LIST=new ListScene();
    LIST.renderInto(document.body);
    if(firstrun)
    {
      LIST.popup(HELP_TEXT);
      saveSettings();
    }

  },
  showMain: function() {
    if(!LIST)LIST=new ListScene();
    LIST.renderInto(document.body);
  }
});

enyo.kind({
	name: "ListScene",
  //kind: 'FittableRows',
  kind: 'Panels',
  classes:"enyo-panels",
  arrangerKind: "LeftRightArranger",
  //arrangerKind: "CollapsingArranger",
  margin:0,
  index:1,
  draggable:false,
  wrap:false,
  fit:true,
	components: [
  {kind: 'ToolsScene', name:'tools'},
    {kind: 'FittableRows', classes:"enyo-fit", components:[
      {kind: "onyx.Toolbar", components: [
        {components:[
          {kind:"FittableColumns", classes:"enyo-fit", style:"padding:6px",components:[
            {kind: "onyx.Button", content:"+",ontap:"download"},
            {fit:true, content:"Nearest notes", style:"text-align:center"},

            {kind: "onyx.MenuDecorator", onSelect: "itemSelected", components: [
              {kind: "onyx.IconButton", src:"assets/images/menu.png"},
              {kind: "onyx.Menu", floating: true, components: [
                {content: "Delete All"},
                {classes: "onyx-menu-divider"},
                {content: "Help"},
                {content: "About"}
              ]}
            ]},
          ]}
        ]}
      ]},
      {name: "popup", kind: 'onyx.Popup', centered: true, floating: true, modal:true,
        allowHtml:true, style: "padding: 10px;"},
      {kind: "List", name: "list", fit:true, enableSwipe:true, classes: "bk", noSelect: true, onSetupItem: "setupItem", fixedHeight: true, components: [
        {name: "item", style: "padding: 10px; max-height:10%", classes: "enyo-border-box list-item" /*,ontap: "itemTap"*/, components: [
          {style:"width:90%;", ontap:"itemTap", components:[
            {layoutKind: "FittableColumnsLayout", components:[
              {name: "id", classes:"id"},
              {classes: "direction", fit:true, classes:"direction", components:[
                {name: "direction", kind: "Image", classes: "small-direction-image"}
              ]},
              {name: "distance", classes:"distance"},
            ]},
            {layoutKind: "FittableColumnsLayout", classes:"comment", components:[
              {name:"nick", classes: "nick"},
              {name:"text", classes: "comment1", style:"margin-right:10px;max-height:1em;"},
            ]},
          ]},
          {kind: "onyx.IconButton", src:"assets/images/remove-icon.png", ontap:"removeItem", classes:"remove-button"},
        ]}
      ]},
      {name: "deleteConfirmation", kind: "onyx.Popup", classes:"popup", centered: true, modal: true, floating: true,components: [
        {content:"Are you sure you want to delete note with your comment?"},
        {tag: "br"},
        {kind: "onyx.Button", style:"float:left", content: "Cancel", ontap: "closePopup"},
        {kind: "onyx.Button", classes:"onyx-affirmative",style: "float:right", content: "Ok", ontap: "delNote"}
      ]},
      {name: "deleteAllConfirmation", kind: "onyx.Popup", classes:"popup", centered: true, modal: true, floating: true, components: [
        {content:"Are you sure you want to delete all notes with ALL YOUR COMMENTS?"},
        {tag: "br"},
        {kind: "onyx.Button", style:"float:left", content: "Cancel", ontap: "closeAllPopup"},
        {kind: "onyx.Button", classes:"onyx-affirmative",style: "float:right", content: "Ok", ontap: "delAllNotes"}
      ]},
    ]},
    {kind: 'CompassScene', name: "compass"},
  ],
	rendered: function() {
		this.inherited(arguments);
    //SETTINGS.radius=10;
    this.start();
  },
  start:function() {
    console.log("Start. targets="+TARGETS.length);
    this.setIndex(1);
    this.$.list.setCount(TARGETS.length);
    this.$.list.refresh();
    this.baseInterval=10000;
    if(TARGETS.length<1)
      this.download();
    else
    {
      this.startTimer();
      if((!this.screenLock)&&window.navigator.requestWakeLock)
        this.screenLock = window.navigator.requestWakeLock('screen');
    }
  },
  create: function() {
    this.inherited(arguments);
    this.screenLock=0;
    window.addEventListener('unload',enyo.bind(this,'clearAll'));
    this.refreshLocation();
  },
  popup: function(message)
  {
    this.$.popup.setContent(message);
    this.$.popup.show();
  },
  reflow: function() {
		this.inherited(arguments);
	},
  startTimer: function()
  {
    this.job = window.setInterval(enyo.bind(this, "refreshLocation"), this.baseInterval);
  },
  refreshLocation: function()
  {
    navigator.geolocation.getCurrentPosition(enyo.bind(this,"updateList"));
  },
  updateList: function(pos)
  {
    //this.log("Log:"+pos.coords.lat+","+pos.coords.lon);
    LOCATION.lat=pos.coords.latitude;
    LOCATION.lon=pos.coords.longitude;
    var angle;
    if(isNaN(pos.coords.heading))
    {
      angle=0;
      DIRECTION.lat=0;
      DIRECTION.lon=0;
    }
    else
    {
      angle=pos.coords.heading/180*Math.PI;
      DIRECTION.lat=Math.cos(angle);
      DIRECTION.lon=Math.sin(angle);
    }
    this.$.list.setCount(TARGETS.length);
    for(var i=0;i<TARGETS.length;i++)
    {
      var t=TARGETS[i];
      t.calcDistance(LOCATION);
      t.calcDirection(LOCATION,DIRECTION);
    }
    TARGETS.sort(function(a,b){return a.distance-b.distance});
    this.$.list.refresh();
  },
  download: function()
  {
    this.clearAll();
    this.$.tools.setPrev(enyo.bind(this,"start"));
    this.$.tools.start();
    this.setIndex(0);
  },
  clearAll:function() {
    console.log("clear all");
    if(this.screenLock)
    {
      this.screenLock.unlock();
      this.screenLock=0;
    }
    window.clearInterval(this.job);
  },
	setupItem: function(inSender, inEvent) {
		var i = inEvent.index;
		var item = TARGETS[i];
		//this.$.item.addRemoveClass("onyx-selected", inSender.isSelected(inEvent.index));
		this.$.direction.setSrc(item.image);
		this.$.id.setContent("#"+item.id);
    this.$.distance.setContent(item.distance+" m");
    this.$.nick.setContent("@"+item.name);
    this.$.text.setContent(item.text);
    if(item.mynote)
    {
      this.$.item.addClass("withnote");
    }
    else
    {
      this.$.item.removeClass("withnote");
    }

		//this.$.more.canGenerate = !this.results[i+1];
	},
  itemSelected: function(inSender, inEvent) {
    //Menu items send an onSelect event with a reference to themselves & any directly displayed content
		if (inEvent.originator.content)
    {
      switch(inEvent.originator.content)
      {
        case "Delete All":
          this.$.deleteAllConfirmation.show();
          break;
        case "Help":
          this.popup(HELP_TEXT);
        break;
        case "About":
          this.popup(ABOUT_TEXT);
        break;
      }
    }
  },
	itemTap: function(inSender, inEvent) {
    //this.stopTimer();
    this.clearAll();
    //if(!COMPASS)COMPASS=new CompassScene();
    //COMPASS.setPrev(this);
    this.$.compass.setTarget(TARGETS[inEvent.index]);
    this.$.compass.setPrev(enyo.bind(this,"start"));
    //COMPASS.renderInto(document.body);
    this.$.compass.start();
    this.setIndex(2);
	},
  removeItem: function(inSender, inEvent) {
    if(!TARGETS[inEvent.index].mynote)
    {
      TARGETS.splice(inEvent.index,1);
      this.$.list.setCount(TARGETS.length);
      this.$.list.refresh();
      saveSettings();
    }
    else
    {
      this.$.deleteConfirmation.index=inEvent.index;
      this.$.deleteConfirmation.show();
    }
  },
  closePopup: function(){
    this.$.deleteConfirmation.hide();
  },
  closeAllPopup: function(){
    this.$.deleteAllConfirmation.hide();
  },
  delAllNotes:function()
  {
    TARGETS=[];
    this.$.list.setCount(TARGETS.length);
    this.$.list.refresh();
    saveSettings();
    this.$.deleteAllConfirmation.hide();
  },
  delNote:function(){
    if(this.$.deleteConfirmation.index>-1)
    {
      TARGETS.splice(this.$.deleteConfirmation.index,1);
      this.$.list.setCount(TARGETS.length);
      this.$.list.refresh();
      saveSettings();
      this.$.deleteConfirmation.index=-1
      this.$.deleteConfirmation.hide();
    }
  }
});

APP=new App();
loadSettings(enyo.bind(APP, "done"));



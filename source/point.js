function Point (lat,lon)
{
  this.lat=lat;
  this.lon=lon;
}

Point.prototype.setPosition = function(lat,lon)
{
  this.lat=lat;
  this.lon=lon;
}


function Target(lat,lon,description)
{
  this.point=new Point(lat,lon);
  this.text=description;
  this.mynote="";
  this.id=0;
  this.name="";
  this.distance=0;
  this.direction=0;
  this.flags={};
  this.image="assets/images/arrows/00.png";
  return this;
}

Target.prototype.copyFrom = function(t)
{
  this.direction=t.direction;
  this.distance=t.distance;
  this.flags=t.flags;
  this.id=t.id;
  this.image=t.image;
//  this.point={};
  this.point.lat=t.point.lat;
  this.point.lon=t.point.lon;
  this.mynote=t.mynote;
  this.name=t.name;
  this.text=t.text;
  return this;
}


Target.prototype.text = function()
{
  return this.text;
}

Target.prototype.setName = function(name)
{
  this.name=name;
}

Target.prototype.setPosition = function (lat,lon)
{
  this.point.setPosition(lat,lon);
}

Target.prototype.setText = function(descr)
{
  this.text=descr;
}

Target.prototype.calcDistance = function(position)
{
  this.distance=Distance(position,this.point);
}

Target.prototype.setNote = function(note)
{
  this.mynote=note;
}

Target.prototype.calcDirection = function(curpos, curdir)
{
  var p=new Point(this.point.lat,this.point.lon);
  p.lat-=curpos.lat;
  p.lon-=curpos.lon;
  if((curdir.lat==0) && (curdir.lon==0))
  {
    this.image="assets/images/arrows/00.png";
    return;
  }
  var d=Angle(p,curdir);
  this.direction=d;
  var step=Math.PI/8;
  var imageid=Math.round(d/step-step/2);
  imageid=-imageid;
  if(imageid<0)
  {
    imageid=16+imageid;
  }
  this.image="assets/images/arrows/"+imageid+".png";

}

function Distance(p1,p2)
{
  var lat1=p1.lat;
  var lat2=p2.lat;
  var lon1=p1.lon;
  var lon2=p2.lon;
  var R = 6378.137; // Radius of earth in KM
    var dLat = (lat2 - lat1) * Math.PI / 180;
    var dLon = (lon2 - lon1) * Math.PI / 180;
    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
    Math.sin(dLon/2) * Math.sin(dLon/2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c;
    return Math.round(d*1000) ; // meters
}

function Angle(a,b)
{
  //return atan2(a.x*b.y-b.x*a.y, a.x*b.x+a.y*b.y);
  var angle=Math.atan2(a.lat*b.lon-b.lat*a.lon,a.lat*b.lat+a.lon*b.lon);
  //Mojo.Log.info("angle=",angle);
  return angle;
}
